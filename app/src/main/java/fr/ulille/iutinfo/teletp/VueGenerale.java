package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment implements AdapterView.OnItemSelectedListener {

    // TODO Q1
    private final String DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
    private String salle;
    private String poste;

    // TODO Q2.c
    private SuiviViewModel model;

    public VueGenerale(int contentLayoutId, String salle, String poste, SuiviViewModel model) {
        super(contentLayoutId);
        this.salle = salle;
        this.poste = poste;
        this.model = model;
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        salle = DISTANCIEL;
        poste = "";
        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spSalle = (Spinner) view.findViewById(R.id.spSalle);
        spSalle.setOnItemClickListener(this::onItemSelected);
        ArrayAdapter<CharSequence> spSalleAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        spSalleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter((spSalleAdapter));

        Spinner spPoste = (Spinner) view.findViewById(R.id.spPoste);
        spPoste.setOnItemClickListener(this::onItemSelected);
        ArrayAdapter<CharSequence> spPosteAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        spPosteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter((spPosteAdapter));

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView userName = view.findViewById(R.id.tvLogin);
            model.setUsername(userName.getText().toString());
            Toast.makeText(getContext(), model.getUsername(), Toast.LENGTH_LONG);
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        update();
        // TODO Q9
    }

    // TODO Q5.a
    private void update(){
        Spinner spPoste = getView().findViewById(R.id.spPoste);
        Spinner spSalle = getView().findViewById(R.id.spSalle);
        if(salle == DISTANCIEL){
            spPoste.setEnabled(false);
            spPoste.setVisibility(View.INVISIBLE);
            model.setLocalisation(DISTANCIEL);
        }else{
            spPoste.setEnabled(true);
            spPoste.setVisibility(View.VISIBLE);
            model.setLocalisation(spSalle.getSelectedItem().toString() +" : "+ spPoste.getSelectedItem().toString());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        update();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
    // TODO Q9
}